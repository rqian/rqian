# for Inclusive LeadingJet in support note
from ROOT import *
import numpy as np

def myText(x,y,text,color=1):
    l = TLatex()
    l.SetTextSize(0.025)
    l.SetNDC()
    l.SetTextColor(color)
    l.DrawLatex(x,y,text)
    pass

var = "pt"

sample = "Inclusive"

jet_type_array = ["LeadingJet_Central","LeadingJet_Forward","SubJet_Central","SubJet_Forward"]

file0 = TFile("../root/dijet_sherpa_inclusive.root")

gamma_data_file = TFile("../root/dijet_data_inclusive_Jan13.root")

gStyle.SetOptStat(0)
# for gamma jet
bin = np.asarray([0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000])
for k in range(1):
    ###Add LeadingJet
    zero_quark = file0.Get("50_"+jet_type_array[0]+"_Quark_pt")
    zero_gluon = file0.Get("50_"+jet_type_array[0]+"_Gluon_pt")
    zero_other = file0.Get("50_"+jet_type_array[0]+"_Other_pt")
    zero_data = gamma_data_file.Get("50_"+jet_type_array[0]+"_Data_pt")
    zero_quark_2 = file0.Get("50_"+jet_type_array[1]+"_Quark_pt")
    zero_gluon_2 = file0.Get("50_"+jet_type_array[1]+"_Gluon_pt")
    zero_other_2 = file0.Get("50_"+jet_type_array[1]+"_Other_pt")
    zero_data_2 = gamma_data_file.Get("50_"+jet_type_array[1]+"_Data_pt")
    
    zero_quark_err = file0.Get("50_"+jet_type_array[0]+"_Quark_pt_err")
    zero_gluon_err = file0.Get("50_"+jet_type_array[0]+"_Gluon_pt_err")
    zero_other_err = file0.Get("50_"+jet_type_array[0]+"_Other_pt_err")
    zero_quark_2_err = file0.Get("50_"+jet_type_array[1]+"_Quark_pt_err")
    zero_gluon_2_err = file0.Get("50_"+jet_type_array[1]+"_Gluon_pt_err")
    zero_other_2_err = file0.Get("50_"+jet_type_array[1]+"_Other_pt_err")
    
    
    #zero_quark_3 = file0.Get("50_"+jet_type_[2]+"_Quark_pt")
    #zero_gluon_3 = file0.Get("50_"+jet_type_[2]+"_Gluon_pt")
    #zero_other_3 = file0.Get("50_"+jet_type_[2]+"_Other_pt")
    #zero_data_3 = gamma_data_file.Get("50_"+jet_type_[1]+"_Data_pt")
    #zero_quark_4 = file0.Get("50_"+jet_type_[3]+"_Quark_pt")
    #zero_gluon_4 = file0.Get("50_"+jet_type_[3]+"_Gluon_pt")
    #zero_other_4 = file0.Get("50_"+jet_type_[3]+"_Other_pt")
    #zero_data_4 = gamma_data_file.Get("50_"+jet_type_[3]+"_Data_pt")
    zero_quark.Add(zero_quark_2)
    #zero_quark.Add(zero_quark_3)
    #zero_quark.Add(zero_quark_4)
    zero_gluon.Add(zero_gluon_2)
    #zero_q.Add(zero_gluon_3)
    #zero_q.Add(zero_gluon_4)
    zero_other.Add(zero_other_2)
    #zero_other.Add(zero_other_3)
    #zero_other.Add(zero_other_4)
    zero_data.Add(zero_data_2)

    zero_quark_err.Add(zero_quark_2_err)
    zero_gluon_err.Add(zero_gluon_2_err)
    zero_other_err.Add(zero_other_2_err)

    for j in range(1,zero_quark_err.GetNbinsX()+1):
        zero_quark.SetBinError(j,np.sqrt(zero_quark_err.GetBinContent(j)))
        zero_gluon.SetBinError(j,np.sqrt(zero_gluon_err.GetBinContent(j)))
        zero_other.SetBinError(j,np.sqrt(zero_other_err.GetBinContent(j)))

    for i in range(2,13):
        gamma_quark = file0.Get(str(bin[i])+"_"+jet_type_array[0]+"_Quark_pt")
        gamma_gluon = file0.Get(str(bin[i])+"_"+jet_type_array[0]+"_Gluon_pt")
        gamma_other = file0.Get(str(bin[i])+"_"+jet_type_array[0]+"_Other_pt")
        gamma_data = gamma_data_file.Get(str(bin[i])+"_"+jet_type_array[0]+"_Data_pt")
        gamma_quark_2 = file0.Get(str(bin[i])+"_"+jet_type_array[1]+"_Quark_pt")
        gamma_gluon_2 = file0.Get(str(bin[i])+"_"+jet_type_array[1]+"_Gluon_pt")
        gamma_other_2 = file0.Get(str(bin[i])+"_"+jet_type_array[1]+"_Other_pt")
        gamma_data_2 = gamma_data_file.Get(str(bin[i])+"_"+jet_type_array[1]+"_Data_pt")
        
        gamma_quark_err = file0.Get(str(bin[i])+"_"+jet_type_array[0]+"_Quark_pt_err")
        gamma_gluon_err = file0.Get(str(bin[i])+"_"+jet_type_array[0]+"_Gluon_pt_err")
        gamma_other_err = file0.Get(str(bin[i])+"_"+jet_type_array[0]+"_Other_pt_err")
        gamma_quark_2_err = file0.Get(str(bin[i])+"_"+jet_type_array[1]+"_Quark_pt_err")
        gamma_gluon_2_err = file0.Get(str(bin[i])+"_"+jet_type_array[1]+"_Gluon_pt_err")
        gamma_other_2_err = file0.Get(str(bin[i])+"_"+jet_type_array[1]+"_Other_pt_err")
        
        
        #gamma_quark_3 = file0.Get("50_"+jet_type_[2]+"_Quark_pt")
        #gamma_gluon_3 = file0.Get("50_"+jet_type_[2]+"_Gluon_pt")
        #gamma_other_3 = file0.Get("50_"+jet_type_[2]+"_Other_pt")
        #gamma_data_3 = gamma_data_file.Get("50_"+jet_type_[1]+"_Data_pt")
        #gamma_quark_4 = file0.Get("50_"+jet_type_[3]+"_Quark_pt")
        #gamma_gluon_4 = file0.Get("50_"+jet_type_[3]+"_Gluon_pt")
        #gamma_other_4 = file0.Get("50_"+jet_type_[3]+"_Other_pt")
        #gamma_data_4 = gamma_data_file.Get("50_"+jet_type_[3]+"_Data_pt")
        gamma_quark.Add(gamma_quark_2)
        #gamma_quark.Add(gamma_quark_3)
        #gamma_quark.Add(gamma_quark_4)
        gamma_gluon.Add(gamma_gluon_2)
        #zero_q.Add(gamma_gluon_3)
        #zero_q.Add(gamma_gluon_4)
        gamma_other.Add(gamma_other_2)
        #gamma_other.Add(gamma_other_3)
        #gamma_other.Add(gamma_other_4)
        gamma_data.Add(gamma_data_2)

        gamma_quark_err.Add(gamma_quark_2_err)
        gamma_gluon_err.Add(gamma_gluon_2_err)
        gamma_other_err.Add(gamma_other_2_err)
        
        for j in range(1,zero_quark_err.GetNbinsX()+1):
            gamma_quark.SetBinError(j,np.sqrt(gamma_quark_err.GetBinContent(j)))
            gamma_gluon.SetBinError(j,np.sqrt(gamma_gluon_err.GetBinContent(j)))
            gamma_other.SetBinError(j,np.sqrt(gamma_other_err.GetBinContent(j)))
        
        zero_other.Add(gamma_other)
        zero_gluon.Add(gamma_gluon)
        zero_quark.Add(gamma_quark)
        zero_data.Add(gamma_data)
    
    zero_gluon.Add(zero_other)
    zero_quark.Add(zero_gluon)
    gamma_total_data = zero_quark.Clone()
    gamma_total = zero_quark.Clone()
    
#    for j in range(1,gamma_total_data.GetNbinsX()+1):
#        print(jet_type,str(bin[i]),j,gamma_data.GetBinCenter(j),gamma_data.GetBinContent(j),gamma_quark.GetBinContent(j)+gamma_gluon.GetBinContent(j)+gamma_other.GetBinContent(j))
 
    for i in range(1,gamma_total_data.GetNbinsX()+1):
            gamma_total_data.SetBinContent(i,zero_data.GetBinContent(i))

    ratio = gamma_total_data.Clone()
    ratio.Divide(gamma_total)

    for i in range(1,gamma_total.GetNbinsX()+1):
        total_mc = int(gamma_total.Integral())
        total_data = int(gamma_total_data.Integral())
    
    ratio.SetTitle("")
    ratio.GetYaxis().SetRangeUser(0.7,1.3)
    ratio.GetXaxis().SetTitle(var)
    ratio.GetXaxis().SetTitleOffset(1)
    ratio.GetXaxis().SetTitleSize(0.11)
    ratio.GetXaxis().SetLabelSize(0.1)
    ratio.GetXaxis().SetLabelOffset(0.03)
    ratio.GetYaxis().SetTitleSize(0.1)
    ratio.GetYaxis().SetTitleOffset(0.5)
    #ratio.GetYaxis().SetLabelSize(0.2)
    ratio.GetYaxis().SetLabelOffset(0.01)
    
    
    #scale by gamma data
    leg2 = TLegend(0.75,0.75,0.95,0.85)
    leg2.AddEntry(gamma_total,"Total", "l")
    leg2.AddEntry(zero_quark,"Quark","f")
    leg2.AddEntry(zero_gluon,"Gluon","f")
    leg2.AddEntry(zero_other,"Other","f")
    leg2.AddEntry(gamma_total_data,"data","lep")
    
    leg2.SetBorderSize(0)
    leg2.SetFillStyle(0)
    c = TCanvas("","",800,800)
    c.Divide(2,1)
    top = c.cd(1)
    top.SetPad(0.0,0.0,1.0,1.0)
    top.SetFillColor(0)
    top.SetBorderMode(0)
    top.SetBorderSize(2)
    top.SetTickx(1)
    top.SetTicky(1)
    top.SetLeftMargin(0.14)
    top.SetRightMargin(0.055)
    top.SetBottomMargin(0.3)#0.25
    top.SetFrameBorderMode(0)
    
    
    bot = c.cd(2)
    bot.SetPad(0.0,0.0,1.0,0.3)
    bot.SetFillColor(0)
    bot.SetBorderMode(0)
    bot.SetBorderSize(2)
    bot.SetTickx(1)
    bot.SetTicky(1)
    bot.SetLeftMargin(0.14)
    bot.SetRightMargin(0.055)
    bot.SetTopMargin(0.045)
    bot.SetBottomMargin(0.4)
    bot.SetFrameBorderMode(0)
    
    top.cd()    
    gPad.SetLogy()
    zero_gluon.SetLineColor(4)
    zero_other.SetLineColor(4)
    zero_gluon.SetLineWidth(3)
    gamma_total.SetLineColor(12)
    gamma_total.SetLineWidth(3)
    zero_quark.SetFillColor(2)
    zero_gluon.SetFillColor(3)
    zero_other.SetFillColor(94)
    gamma_total_data.SetLineColor(1)
    gamma_total_data.SetLineWidth(3)
    gamma_total_data.SetMarkerStyle(20)
    gamma_total_data.SetMarkerSize(0.5)

    ratio.GetXaxis().SetTitle('LeadingJet p_{T} [GeV]')
    ratio.GetXaxis().SetRangeUser(50.,2000.)
    zero_quark.GetXaxis().SetRangeUser(50.,2000.)
    zero_quark.GetYaxis().SetTitle('Events')    
    zero_quark.Draw("Hist")
    zero_gluon.Draw("HIST  same")
    gamma_total.Draw("HIST same")
    zero_other.Draw("HIST same")
    gamma_total_data.Draw("E1P same")
    
    leg2.Draw("same")
    myText(0.3,0.84,'#it{#bf{#scale[1.4]{#bf{ATLAS} Simulation Preliminary}}}')
    #myText(0.3,0.81,"#bf{#scale[1.0]{#bf{dijet }"+ str(sample)+"}}")
    #myText(0.3,0.78,"#bf{#scale[1.0]{mc entries:" + str(total_mc) + "}}" +" "+ "#bf{#scale[1.0]{data entries:" + str(total_data) + "}}")
    #c.Print("./plots/fraction_plots/gamma_distribution_"+var+"_sherpa.pdf")
    bot.cd()
    ratio.Draw("E")
    ratio.GetYaxis().SetTitle('Data/Mc')
    ratio.GetYaxis().SetLabelSize(0.1)
    line = TLine(50,1,2000,1)
    line.Draw("same")
    c.Print( "LeadingJet_Dijet_"+sample+ ".png")
    #c.Print("./plots/fraction_plots/gamma_distribution
