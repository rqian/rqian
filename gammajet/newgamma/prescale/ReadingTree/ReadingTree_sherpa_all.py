#!/usr/bin/env python
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import uproot3 as uproot
import numpy as np
import sys

#bins = [300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]  #this bin range is for only dijet event
bins = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]  #this bin range is for gammajet+dijet event
HistMap = {}
JetList = []

xsec = [0.0,21400000000.0,89314000.0,9275100.0,55101.0,1631.4,128.4,27.212,0.206,0.036,0.0,0.0,0.0]
eff = [0.0,0.0014414,0.0051224,0.00056516,0.0014972,0.024206,0.01082,0.003618,0.015966,0.0033033,0.0,0.0,0.0]
	   
finput = open("/afs/cern.ch/user/r/rqian/public/NewReadingTree/dijet/dijet-sherpa.txt")
inputs = finput.read().splitlines()
nentries_a = [0.0,4997600,4990400,14955400,14962200,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
nentries_d =[0.0,14981000,4976000,14584000 ,14953000,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
nentries_e =[0.0,24868000 ,14959000, 9967000,9968000,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]

usefiles = []

for file in inputs:
    tr = uproot.open(file)["AntiKt4EMPFlow_dijet_insitu"]
    data = tr.arrays(["mcChannelNumber","weight"])
    print(data)
    index = (data[b"mcChannelNumber"][0]-76) % 10
    print(file,index)
    if(index<=4):
        if (".a." in file or "sherpaA" in file):
            run = "a"
        if (".d." in file or "sherpaD" in file):
            run = "d"
        if (".e." in file or "sherpaE" in file):
            run = "e"
    if(index > 4):
        mc_mod = data[b"mcChannelNumber"][0] % 10
        entries = uproot.open(file)["AntiKt4EMPFlow_J"+str(mc_mod)+"_sumOfWeights"].values[0]
        #sumofev += entries
        if (".a." in file or "sherpaA" in file):
            run = "a"
            nentries_a[index] +=entries  
        if (".d." in file or "sherpaD" in file):
            run = "d"
            nentries_d[index] +=entries  
        if (".e." in file or "sherpaE" in file):
            run = "e"
            nentries_e[index] +=entries 
    if(index == int(sys.argv[1]) and run == str(sys.argv[2])):
        usefiles.append(file)

run = str(sys.argv[2])
    
print(nentries_a)
print(nentries_d)
print(nentries_e)


###### define functions
def GetHistBin(histname):
	if 'pt' in histname:
		return 60,0,2000
	elif 'eta' in histname:
		return 50,-2.5,2.5
	elif 'ntrk' in histname:
		return 60,0,60
	elif 'bdt' in histname:
		return 60,-0.8,0.7
	elif 'width' in histname:
		return 60,0.,0.4
	elif 'c1' in histname:
		return 60,0.,0.4

def FillTH1F(histname, var, w):
    if 'Data' in histname:
        w = 1
    if histname in HistMap:
        HistMap[histname][0].append(var)
        HistMap[histname][1].append(w) 
    else:
                HistMap[histname] = [[],[]] #The first list is for the data, the second for the weights
                HistMap[histname] = [[],[]]
                HistMap[histname][0].append(var)
                HistMap[histname][1].append(w)

def FillHisto(prefix, jetlist, w):
	FillTH1F(prefix+"_ntrk", jetlist[0], w)
	FillTH1F(prefix+"_bdt", jetlist[1], w)
	FillTH1F(prefix+"_width", jetlist[2], w)
	FillTH1F(prefix+"_c1", jetlist[3], w)
	FillTH1F(prefix+"_pt", jetlist[4], w)
	FillTH1F(prefix+"_eta", jetlist[5], w)


def GetJetType(label):
	if label == -99:
		return "Data"
	elif label == 21:
		return "Gluon"
	elif label > 0 and label < 5:
		return "Quark"
	else:
		return "Other"


def FindBinIndex(jet_pt,ptbin):
	for j in range(len(ptbin)-1):
		if jet_pt >= ptbin[j] and jet_pt < ptbin[j+1]:
			return ptbin[j]

	print("error: jet pT outside the bin range")
	return -1

#Unfourtunately I can't fully utilize the use of arrays because each jet must be matched with the corresponding histogram.
#for i in range():
def ReadTree(df):
    if run == "a":
        nentries = nentries_a
        lumi = 36000
    if run == "d":
        nentries = nentries_d
        lumi =  44500
    if run == "e":
        nentries = nentries_e
        lumi = 58500
    for i in range(0,len(df[b"pass_HLT_j400"])):
        if(df[b"j1_pT"][i] > 40 and df[b"j1_pT"][i] < 2000 and abs(df[b"j1_eta"][i]) < 2.1 and abs(df[b"j2_eta"][i]) < 2.1 and df[b"j1_pT"][i]/df[b"j2_pT"][i] < 1.5):
            
            pTbin1 = FindBinIndex(df[b"j1_pT"][i], bins)
            pTbin2 = FindBinIndex(df[b"j2_pT"][i], bins)
            
            label1 = GetJetType(df[b"j1_partonLabel"][i])
            label2 = GetJetType(df[b"j2_partonLabel"][i])
            
            eta1 = "Central"
            eta2 = "Forward"
            if abs(df[b"j1_eta"][i]) > abs(df[b"j2_eta"][i]):
            	eta1 = "Forward"
            	eta2 = "Central"
            
            JetList = [[df[b"j1_NumTrkPt500"][i], df[b"j1_bdt_resp"][i], df[b"j1_trackWidth"][i], df[b"j1_trackC1"][i], df[b"j1_pT"][i], df[b"j1_eta"][i]],[df[b"j2_NumTrkPt500"][i],df[b"j2_bdt_resp"][i], df[b"j2_trackWidth"][i], df[b"j2_trackC1"][i], df[b"j2_pT"][i], df[b"j2_eta"][i]]]
            
            total_weight = lumi * df[b"weight"][i]*xsec[int(sys.argv[1])]*eff[int(sys.argv[1])]/nentries[int(sys.argv[1])]
            	
            FillHisto(str(pTbin1)+"_LeadingJet_"+eta1+"_"+label1, JetList[0], total_weight)
            FillHisto(str(pTbin2)+"_SubJet_"+eta2+"_"+label2, JetList[1], total_weight)


######## read and excute TTree from root file 
#finput = TFile.Open("/dijet_sherpa_bdt_d.root")

#print(usefiles)
for file in usefiles:
    print(file)
    tr = uproot.open(file)["AntiKt4EMPFlow_dijet_insitu"]
    data = tr.arrays(["pass_HLT_j400","j[12]_pT","j[12]_eta","j[12]_NumTrkPt500","j[12]_bdt_resp","j[12]_trackWidth","j[12]_trackC1","weight","j[12]_partonLabel"])#this converts the tree to a     python dictionary with keys equal to the branch names and values equal to the branch data. This can be left blank to iport all of the data from all branches.
    ReadTree(data)

foutput = uproot.recreate(sys.argv[3])

#Create the actual histograms now that the data is in separate lists
#uproot lets you use numpy histograms and write them to root files.
for hist in HistMap.keys():
    #print(HistMap)
    nbin,binmin,binmax = GetHistBin(hist)
    histogram = np.histogram(a = HistMap[hist][0], weights = HistMap[hist][1], bins = nbin, range = (binmin,binmax))
    #print(histogram)
    foutput[hist] = histogram
    
    weight = np.array(HistMap[hist][1])
    binning = np.linspace(binmin,binmax,nbin)
    sum_w2 = np.zeros([nbin], dtype=np.float32)
    digits = np.digitize(HistMap[hist][0],binning)
    for i in range(nbin):
        weights_in_current_bin = weight[np.where(digits == i)[0]]
        sum_w2[i] = np.sum(np.power(weights_in_current_bin, 2))
    #print(sum_w2)
    histogram_err = np.histogram(a = binning, weights = sum_w2, bins = nbin, range = (binmin,binmax))
    foutput[hist+"_err"] = histogram_err


