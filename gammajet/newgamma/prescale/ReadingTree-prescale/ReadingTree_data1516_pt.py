from ROOT import *
#import numpy as np
import sys
import os
import itertools

# This scripts read ttree as inputs and produce different histograms of distribution of variables in different pT range including unprescale data

#1.Read the luminosity table
#2.Loop over HLT by sys.argv[1]
#2.Select event :EMPFlowAntiKti4 leading jet pt/sub-leading jet  pt < 1.5,  both jets |eta|<2.1
#3.decide whether the jet pt is in the thershold of each trigger
#4.for each leading jet pT range, determinate forward or central assignment to 1st and 2nd jet, then categorise each jet as quark/gluon/other or  data
#5.fill hist with variables :Ntrk / BDT

# TODO: confirm whether HLT_j400 or HLT_j420 is the unperscale trigger

#v2 is Tancredi's original luminosity, v3 delete the triggers that miss some events to get smooth distribution
lumifile_name = "/afs/cern.ch/user/r/rqian/public/ReadingTree-ps/ReadingTree-New/Lumi_datahighmu20" +sys.argv[2] + "_v3.txt"

# HLT in the luminosity table

# a bit complicated because there is some misorder in the naming, so need to record the index corresponding to each trigger.
# name of HLT stored in ntuple
HLT_store_ntuple= ["HLT_j15","HLT_j25","HLT_j35","HLT_j45","HLT_j55","HLT_j60","HLT_j85","HLT_j110","HLT_j150","HLT_j175","HLT_j260","HLT_j420"]

# name of HLT stored in ntuple. HLT_j400 is unprescale trigger in ntuple but I question it should be HLT_j420
HLT_store_ntuple_420 = ["HLT_j15","HLT_j25","HLT_j35","HLT_j45","HLT_j55","HLT_j60","HLT_j85","HLT_j110","HLT_j150","HLT_j175","HLT_j260","HLT_j380"]


#bins = [300, 400, 500, 600, 800, 1000, 1200, 1500, 2000] #for method 1
bins = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]
HistMap = {}
JetList = []

###### define functions

def GetTriggerPrescaleFactor(Lumiwgt,HLT_list):
    possibility = 0
    sign_pass_combination = itertools.combinations(HLT_list,1)
    for i in sign_pass_combination:
        for j in i:
            #print(j,Lumiwgt[j]["prescale_factor"])
            possibility += 1/Lumiwgt[j]["prescale_factor"]
    for pass_number in range(2,len(HLT_list)+1):
        multi_pass_combination = itertools.combinations(HLT_list,pass_number)
        for triggers in multi_pass_combination:
            possibility_initial = 1 
            for trigger in triggers:
                possibility_initial *= 1/Lumiwgt[trigger]["prescale_factor"]
                print(possibility_initial)
            possibility -= possibility_initial
    return(1/possibility)

def GetSingleTriggerPrescaleFactor(trigger_name):
    return Lumiwgt[trigger_name]["prescale_factor"]


def GetPtNominal(trigname):
        sptnom = triggerinfo[0]
        sptnom = sptnom.replace("HLT_j","")
        sptnom = sptnom.replace("_perf_ds1_L1J","")
        sptnom = sptnom.replace("_perf_L1RD0_FILLED","")
        sptnom = sptnom.replace("_320eta490","")
        return int(sptnom)

def GetHistBin(histname):
	if 'pt' in histname:
		return 60,0,2000
	elif 'eta' in histname:
		return 50,-2.5,2.5
	elif 'ntrk' in histname:
		return 60,0,60
	elif 'bdt' in histname:
		return 60,-0.8,0.7
	elif 'width' in histname:
		return 60,0.,0.4
	elif 'c1' in histname:
		return 60,0.,0.4

def FillTH1F(histname, var, w):
	if histname in HistMap:
		HistMap[histname].Fill(var, w)
	else:
		nbin,bmin,bmax = GetHistBin(histname)
		HistMap[histname]= TH1F(histname,"", nbin, bmin, bmax)
		HistMap[histname].Fill(var, w)


def FillHisto(prefix, jetlist, w):
	FillTH1F(prefix+"_ntrk", jetlist[0], w)
	FillTH1F(prefix+"_bdt", jetlist[1], w)
	FillTH1F(prefix+"_width", jetlist[2], w)
	FillTH1F(prefix+"_c1", jetlist[3], w)
	FillTH1F(prefix+"_pt", jetlist[4], w)
	FillTH1F(prefix+"_eta", jetlist[5], w)


def GetJetType(label):
	if label == -99:
		return "Data"
	elif label == 21:
		return "Gluon"
	elif label > 0 and label < 5:
		return "Quark"
	else:
		return "Other"


def FindBinIndex(jet_pt,ptbin):
	for i in range(len(ptbin)-1):
		if jet_pt >= ptbin[i] and jet_pt < ptbin[i+1]:
			return ptbin[i]

	#print "error: jet pT ",jet_pt,"outside the bin range"
	return -1

########read luminosity table

lumifile = open(lumifile_name,"r")
#record the number of HLT trigger

Lumiwgt_size = 0 
Lumiwgt={}
for tiggerline in lumifile.readlines():
    Lumiwgt_size+=1
    triggerinfo = tiggerline.split()
    #print(triggerinfo)
    if len(triggerinfo)== 0:
        break
    if not("#" in triggerinfo[0]):
        Lumiwgt[triggerinfo[0]] = {'lumi':eval(triggerinfo[1]), 'ptnom': GetPtNominal(triggerinfo[0]),'ptmin':float(triggerinfo[2])}
lumifile.close()    

lowest_central_trigger = 1
lowest_forward_trigger = 1

# select the trigger with highest thershold and luminosity
lumifile = open(lumifile_name,"r")
for tiggerline in lumifile.readlines():
    triggerinfo = tiggerline.split()
    if (not("#" in triggerinfo[0]) and not("eta" in triggerinfo[0])):
        if lowest_central_trigger == 0:
            Lumiwgt[last_trigger_key]["ptmax"] = triggerinfo[2] 
            last_trigger_key = triggerinfo[0]
        else:
            lowest_central_trigger =0
            last_trigger_key = triggerinfo[0] 
        Lumiwgt["highest_central_trigger"] = {'lumi':eval(triggerinfo[1]), 'ptnom': GetPtNominal(triggerinfo[0]),'ptmin':float(triggerinfo[2])}
    if (not("#" in triggerinfo[0]) and ("eta" in triggerinfo[0])):       
        if lowest_forward_trigger == 0:
            Lumiwgt[last_trigger_key]["ptmax"] = triggerinfo[2] 
            last_trigger_key = triggerinfo[0]
        else:
            lowest_forward_trigger =0
            last_trigger_key = triggerinfo[0] 
        Lumiwgt["highest_forward_trigger"] = {'lumi':eval(triggerinfo[1]), 'ptnom': GetPtNominal(triggerinfo[0]),'ptmin':float(triggerinfo[2])}
lumifile.close()    

for trigger_name in Lumiwgt.keys():
    #print(trigger_name)
    if not("eta" in triggerinfo):
        Lumiwgt[trigger_name]["prescale_factor"] = Lumiwgt["highest_central_trigger"]["lumi"]/Lumiwgt[trigger_name]["lumi"]
    if ("eta" in triggerinfo):
        Lumiwgt[trigger_name]["prescale_factor"] = Lumiwgt["highest_forward_trigger"]["lumi"]/Lumiwgt[trigger_name]["lumi"]
    if Lumiwgt[trigger_name]["prescale_factor"] == 1:
        Lumiwgt[trigger_name]["ptmax"] = 999999

########finish reading luminosity table,check it 

for triggers in Lumiwgt:
    print(triggers,Lumiwgt[triggers])

######## select those triggers both in luminosity table and ntuple

HLT_use_array = []
HLT_index_array = []
trigger_n = -1
for key in HLT_store_ntuple_420:
    trigger_n += 1
    if Lumiwgt.get(key):
        HLT_use_array.append(key)
        HLT_index_array.append(trigger_n)

print("HLT stored in ntuple:")
print(HLT_store_ntuple)
print("HLT stored in both ntuple and luminosity table:")
print(HLT_use_array)
print("HLT stored in both ntuple and luminosity table's index in HLT stored in ntuple array:")
print(HLT_index_array)


print("start reading tree!")


######## read and excute TTree from root file
#finput = TFile.Open("/eos/user/w/wasu/AQT_dijet_sherpa_bdt/dijet_sherpa_bdt.root")
finput = TFile.Open("/eos/home-r/rqian/dijet-data-bdt/data"+sys.argv[2]+"_Jan3.root")
t1 = finput.Get("AntiKt4EMPFlow_dijet_insitu")
for i in t1:
		HLT_array = [i.pass_HLT_j15,i.pass_HLT_j25,i.pass_HLT_j35,i.pass_HLT_j45,i.pass_HLT_j55,i.pass_HLT_j60,i.pass_HLT_j85,i.pass_HLT_j110,i.pass_HLT_j150,i.pass_HLT_j175,i.pass_HLT_j260,i.pass_HLT_j420]
		HLT_use= [HLT_store_ntuple_420[int(sys.argv[1])]]
		if HLT_array[int(sys.argv[1])] == 1 and  i.j1_pT > 40 and i.j1_pT < 2000 and abs(i.j1_eta) < 2.1 and abs(i.j2_eta) < 2.1 and i.j1_pT/i.j2_pT < 1.5:
			pTbin1 = FindBinIndex(i.j1_pT, bins)
			pTbin2 = FindBinIndex(i.j2_pT, bins)
			
			JetList = [[i.j1_NumTrkPt500, i.j1_bdt_resp, i.j1_trackWidth, i.j1_trackC1, i.j1_pT, i.j1_eta],[i.j2_NumTrkPt500,i.j2_bdt_resp, i.j2_trackWidth, i.j2_trackC1, i.j2_pT, i.j2_eta]] # JetList[0] for 1st jet, JetList[1] for 2nd jet

			label1 = GetJetType(i.j1_partonLabel)
			label2 = GetJetType(i.j2_partonLabel)

			eta1 = "Central"
			eta2 = "Forward"
			if abs(i.j1_eta) > abs(i.j2_eta):
				eta1 = "Forward"
				eta2 = "Central"
			weight_all = GetTriggerPrescaleFactor(Lumiwgt,HLT_use)
			#print("testflag:",i.j1_pT,HLT_use,weight_all,Lumiwgt[HLT_store_ntuple_420[int(sys.argv[1])]]["ptmax"])
			if i.j1_pT>=float(Lumiwgt[HLT_store_ntuple_420[int(sys.argv[1])]]["ptmin"]) and i.j1_pT<float(Lumiwgt[HLT_store_ntuple_420[int(sys.argv[1])]]["ptmax"]):
				FillHisto(str(pTbin1)+"_LeadingJet_"+eta1+"_"+label1, JetList[0], weight_all)
			if i.j2_pT>=float(Lumiwgt[HLT_store_ntuple_420[int(sys.argv[1])]]["ptmin"]) and i.j2_pT<float(Lumiwgt[HLT_store_ntuple_420[int(sys.argv[1])]]["ptmax"]):
				FillHisto(str(pTbin2)+"_SubJet_"+eta2+"_"+label2, JetList[1], weight_all)


foutput = TFile("/afs/cern.ch/user/r/rqian/public/ReadingTree-ps/ReadingTree-New/out/dijet_data"+sys.argv[2]+"_"+HLT_store_ntuple_420[int(sys.argv[1])]+".root","recreate")
#foutput = TFile("dijet_sherpa_py_forGamma.root","recreate")
for hist in HistMap.values():
	#for i in range(len(bins)):
		#if str(bins[i]) in HistMap.keys():
		#	td = foutput.mkdir("pT: "+str(bins[i])+"-"+str(bins[i+1]))
		#	td.cd()
	hist.Write()

foutput.Write()
foutput.Close()

