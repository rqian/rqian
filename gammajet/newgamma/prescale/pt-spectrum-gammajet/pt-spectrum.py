# inclusive distribution used in the note
from ROOT import *
import numpy as np

def myText(x,y,text,color=1):
    l = TLatex()
    l.SetTextSize(0.025)
    l.SetNDC()
    l.SetTextColor(color)
    l.DrawLatex(x,y,text)
    pass

var = "pt"

sample = "GammaJet_Sherpa"

jet_type_array = ["LeadingJet_Central"]

file0 = TFile("../root/gammajet_sherpa_inclusive_Jan3.root")

gamma_data_file = TFile("../root/gammajet_data_inclusive.root")

gStyle.SetOptStat(0)
# for gamma jet
bin = np.asarray([0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000])
for jet_type in jet_type_array:
    zero_quark = file0.Get("0_"+jet_type+"_Quark_pt")
    zero_gluon = file0.Get("0_"+jet_type+"_Gluon_pt")
    zero_other = file0.Get("0_"+jet_type+"_Other_pt")
    zero_data = gamma_data_file.Get("0_"+jet_type+"_Data_pt")
    
    for i in range(1,13):
        gamma_quark = file0.Get(str(bin[i])+"_"+jet_type+"_Quark_"+var)
        gamma_gluon = file0.Get(str(bin[i])+"_"+jet_type+"_Gluon_"+var)
        gamma_other = file0.Get(str(bin[i]) + "_"+jet_type+"_Other_" +var)
        gamma_data = gamma_data_file.Get(str(bin[i]) + "_"+jet_type+"_Data_" +var)
        
        zero_other.Add(gamma_other)
        zero_gluon.Add(gamma_gluon)
        zero_quark.Add(gamma_quark)
        zero_data.Add(gamma_data)
    
    
    zero_gluon.Add(zero_other)
    zero_quark.Add(zero_gluon)
    gamma_total = zero_quark.Clone()
    gamma_total_data = zero_data.Clone()
    
    for i in range(1,gamma_total_data.GetNbinsX()+1):
            gamma_total_data.SetBinContent(i,zero_data.GetBinContent(i))
    print(jet_type)   
    
    ratio = gamma_total_data.Clone()
    ratio.Divide(gamma_total)

    for i in range(1,gamma_total.GetNbinsX()+1):
        total_mc = int(gamma_total.Integral())
        total_data = int(gamma_total_data.Integral())    
    
    ratio.SetTitle("")
    ratio.GetYaxis().SetRangeUser(0.7,1.3)
    ratio.GetXaxis().SetTitle(var)
    ratio.GetXaxis().SetTitleOffset(1)
    ratio.GetXaxis().SetTitleSize(0.11)
    ratio.GetXaxis().SetLabelSize(0.1)
    ratio.GetXaxis().SetLabelOffset(0.03)
    ratio.GetYaxis().SetTitleSize(0.1)
    ratio.GetYaxis().SetTitleOffset(0.5)
    #ratio.GetYaxis().SetLabelSize(0.2)
    ratio.GetYaxis().SetLabelOffset(0.01)
    
    #scale by gamma data
    leg2 = TLegend(.75,0.75,0.95,0.85)
    leg2.AddEntry(gamma_total,"Total", "l")
    leg2.AddEntry(zero_quark,"Quark","f")
    leg2.AddEntry(zero_gluon,"Gluon","f")
    leg2.AddEntry(zero_other,"Other","f")
    leg2.AddEntry(gamma_total_data,"data","lep")
    
    leg2.SetBorderSize(0)
    leg2.SetFillStyle(0)
    c = TCanvas("","",500,500)
    c = TCanvas("","",800,800)
    c.Divide(2,1)
    top = c.cd(1)
    top.SetPad(0.0,0.0,1.0,1.0)
    top.SetFillColor(0)
    top.SetBorderMode(0)
    top.SetBorderSize(2)
    top.SetTickx(1)
    top.SetTicky(1)
    top.SetLeftMargin(0.14)
    top.SetRightMargin(0.055)
    top.SetBottomMargin(0.3)#0.25
    top.SetFrameBorderMode(0)
    
    
    bot = c.cd(2)
    bot.SetPad(0.0,0.0,1.0,0.3)
    bot.SetFillColor(0)
    bot.SetBorderMode(0)
    bot.SetBorderSize(2)
    bot.SetTickx(1)
    bot.SetTicky(1)
    bot.SetLeftMargin(0.14)
    bot.SetRightMargin(0.055)
    bot.SetTopMargin(0.045)
    bot.SetBottomMargin(0.4)
    bot.SetFrameBorderMode(0)
    
    top.cd()    
    gPad.SetLogy()
    zero_gluon.SetLineColor(4)
    zero_other.SetLineColor(4)
    zero_gluon.SetLineWidth(3)
    gamma_total.SetLineColor(12)
    gamma_total.SetLineWidth(3)
    zero_quark.SetFillColor(2)
    zero_gluon.SetFillColor(3)
    zero_other.SetFillColor(94)
    gamma_total_data.SetLineColor(1)
    gamma_total_data.SetLineWidth(3)
    gamma_total_data.SetMarkerStyle(20)
    gamma_total_data.SetMarkerSize(0.5)

    ratio.GetXaxis().SetTitle('Jet p_{T} [GeV]')
    ratio.GetXaxis().SetRangeUser(50.,2000.)
    zero_quark.GetXaxis().SetRangeUser(50.,2000.)
    zero_quark.GetYaxis().SetTitle('Events')    
    zero_quark.Draw("Hist")
    zero_gluon.Draw("HIST  same")
    gamma_total.Draw("HIST same")
    zero_other.Draw("HIST same")
    gamma_total_data.Draw("E1P same")
    
    leg2.Draw("same")
    myText(0.3,0.84,'#it{#bf{#scale[1.4]{#bf{ATLAS} Internal}}}')
    myText(0.3,0.80,"#bf{#scale[1.4]{#sqrt{s}=13 GeV}}")
    #myText(0.3,0.78,"#bf{#scale[1.0]{mc entries:" + str(total_mc) + "}}" +" "+ "#bf{#scale[1.0]{data entries:" + str(total_data) + "}}")
    #c.Print("./plots/fraction_plots/gamma_distribution_"+var+"_sherpa.pdf")
    bot.cd()
    ratio.SetLineColor(1)
    ratio.Draw("E")
    ratio.GetYaxis().SetTitle('Data/Mc')
    ratio.GetYaxis().SetLabelSize(0.1)
    line = TLine(50,1,2000,1)
    line.Draw("same")
    c.Print(sample+ "_pt_distribution.pdf")
    #c.Print("./plots/fraction_plots/gamma_distribution
