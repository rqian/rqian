from ROOT import *
import numpy as np
import os
import collections

mc_channel_no = [364677,364678,364679,364680,364681,364682,364683,364684,364685]
xsec = [2.1406E+10,8.9314E+07,9.2751E+06,5.5101E+04,1.6314E+03,1.2842E+02,2.7212E+01,2.0583E-01,3.5683E-02]
#pb
gen_Fli = [1.4419E-03,5.1230E-03,5.6541E-04,1.4974E-03,2.4206E-02,1.0820E-02,3.6180E-03,1.5966E-02,3.3033E-03]

def FindBinIndex(jet_pt,ptbin):
        for i in range(len(ptbin)-1):
                if jet_pt >= ptbin[i] and jet_pt < ptbin[i+1]:
                        return i

        #print "error: jet pT ",jet_pt,"outside the bin range"
        return -1

bins = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]

A = np.zeros(13)
B = np.zeros(13)
C = np.zeros(13)
D = np.zeros(13)
f = open("/afs/cern.ch/user/r/rqian/public/gamma2jet/ABCD/readvar/dijet.txt","r")
data = []

for line in f:
    data.append(line[:-1])
f.close()

nentries = np.zeros(9)

for j in data:
    print(j)
    for i in range(len(mc_channel_no)):
        if ('.' + str(mc_channel_no[i])) in j:
            index = i
    open_file = TFile(str(j),"r")
    file_tree = open_file.nominal
    entries = file_tree.GetEntries()
    nentries[index] += entries
print(nentries)

for j in data:
    print(j)
    for i in range(len(mc_channel_no)):
        if ('.' + str(mc_channel_no[i])) in j:
            ind = i

    open_file = TFile(str(j),"r")
    file_tree = open_file.nominal
    for i in file_tree:
        if len(i.ph_baseline_isotool_pass_fixedcuttight)!= 0 and len(i.jet_pt)>1 and i.jet_pt[0]/1000 > 40 and i.jet_pt[0]/1000 < 2000 and abs(i.jet_eta[0]) < 2.1 and abs(i.jet_eta[1]) < 2.1 and i.jet_pt[0]/i.jet_pt[1] <1.5 :
            total_weight = 139000*gen_Fli[ind]*i.pu_weight*xsec[ind]*i.mconly_weight/nentries[ind]
            if len(i.ph_baseline_isotool_pass_fixedcuttight)!=0 :
                index = FindBinIndex((i.jet_pt[0])/1000,bins)
                if i.ph_baseline_ph_isTight[0] == 0 and i.ph_baseline_ph_isMedium[0] == 0 and i.ph_baseline_isotool_pass_fixedcuttight[0] == 0:
                    D[index] += total_weight
                if i.ph_baseline_ph_isTight[0] == 1 and i.ph_baseline_isotool_pass_fixedcuttight[0] == 0:
                    B[index] += total_weight
                if i.ph_baseline_ph_isTight[0] == 0 and i.ph_baseline_ph_isMedium[0] == 0 and i.ph_baseline_isotool_pass_fixedcuttight[0] == 1:
                    C[index] += total_weight
                if i.ph_baseline_ph_isTight[0] ==1 and i.ph_baseline_isotool_pass_fixedcuttight[0] == 1:
                    A[index] += total_weight

print("result:")
print(A)
print(B)
print(C)
print(D)
