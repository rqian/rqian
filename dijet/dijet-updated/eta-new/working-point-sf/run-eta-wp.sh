for wp in {0.5,0.6,0.7,0.8}
do
    for var in {"ntrk","bdt"}
    do 
         for eta in {"0-5_10-15_"}
         do
             echo $wp $var $eta
             python eta-wp.py $wp $var $eta       
         done
    done
done
