#distribution 
from ROOT import *
import numpy as np
import uncertainties as unc # propagate uncertainties
from uncertainties import unumpy as unp # array operations for type ufloat
MC_type = "sherpa"

doreweight = "Quark"   #decide if we want to do the reweighting process

var = "ntrk"  #change the var name according to the inputvar you want to read
mc = "sherpa_SF"   #by setting it as "SF" or "MC", it will automatically making scale factor plots or MC closure plots
inputvar = "ntrk"  #by setting it as bdt (or ntrk,width,c1..), it will read the corresponding histogram, but remember to change the TLine range according to X-axis of different variable, one can check it by browsing the histograms in root file.



ntrackall = TFile("../../newroot/sherpa_all_Jan27.root")
ntrackall3  = TFile("../../newroot/dijet_data_Jan27.root")


def myText(x,y,text,color =1):
    l = TLatex()
    l.SetTextSize(0.025)
    l.SetNDC()
    l.SetTextColor(color)
    l.DrawLatex(x,y,text)
    pass


#convert histogram and error into unp.uarray
#if sample is pyroot input, GetBinError returns correct result
def unc_array(hist):
    value = np.zeros(hist.GetNbinsX())
    error = np.zeros(hist.GetNbinsX())
    for j in range(1,hist.GetNbinsX()+1):
        value[j-1] = hist.GetBinContent(j)
        error[j-1] = hist.GetBinError(j)
    result = unp.uarray(value,error)
    return(result)

#convert histogram and error into unp.uarray
#if sample is uproot input, err is the sumw2 of the corresponding histogram
def unc_array_err(hist,err):
    value = np.zeros(hist.GetNbinsX())
    error = np.zeros(hist.GetNbinsX())
    for j in range(1,hist.GetNbinsX()+1):
        value[j-1] = hist.GetBinContent(j)
        error[j-1] = np.sqrt(err.GetBinContent(j))
    result = unp.uarray(value,error)
    return(result)

def set_hist_error(hist,unc):
    for i in range(1,hist.GetNbinsX()+1):
        hist.SetBinError(i,unp.std_devs(unc[i-1]))

bin = [0,50,100,150,200,300,400,500,600,800,1000,1200,1500,2000]
for k in range(7,13):   #for only dijet event, start from jet pT>500 GeV
#for i in range(13):	#for gamma+jet combined with dijet event, start from jet pT>0 GeV
        min = bin[k]
        max = bin[k+1]
        higher_quark2 = ntrackall.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar)
        higher_gluon2 = ntrackall.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar)
        higher_data2 = ntrackall3.Get(str(min)+"_LeadingJet_Forward_Data_"+inputvar)
        lower_quark2 = ntrackall.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar)
        lower_gluon2 = ntrackall.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar)
        lower_data2 = ntrackall3.Get(str(min)+"_LeadingJet_Central_Data_"+inputvar)

        higher_quark = ntrackall.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar)
        higher_gluon = ntrackall.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar)

        lower_quark = ntrackall.Get(str(min)+"_SubJet_Central_Quark_"+inputvar)
        lower_gluon = ntrackall.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar)

        higher_data = ntrackall3.Get(str(min)+"_SubJet_Forward_Data_"+inputvar)
        lower_data = ntrackall3.Get(str(min)+"_SubJet_Central_Data_"+inputvar)

        c = TCanvas("c","c",800,800)

        #add leading and subleading jet from only dijet event together,
        #note that for gammajet+dijet event, we need to add leading jet from gammajet and leading jet from dijet sample together
        higher_data.Add(higher_data2)
        lower_data.Add(lower_data2)
        higher_quark.Add(higher_quark2)
        higher_gluon.Add(higher_gluon2)
        lower_quark.Add(lower_quark2)
        lower_gluon.Add(lower_gluon2)
    
        higher_quark2_err = ntrackall.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar + "_err")
        higher_gluon2_err = ntrackall.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar + "_err")
        lower_quark2_err = ntrackall.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar + "_err")
        lower_gluon2_err = ntrackall.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar + "_err")

        higher_quark_err = ntrackall.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar + "_err")
        higher_gluon_err = ntrackall.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar + "_err")

        lower_quark_err = ntrackall.Get(str(min)+"_SubJet_Central_Quark_"+inputvar + "_err")
        lower_gluon_err = ntrackall.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar + "_err")



        #add leading and subleading jet from only dijet event together,
        #note that for gammajet+dijet event, we need to add leading jet from gammajet and leading jet from dijet sample together

        higher_quark_err.Add(higher_quark2_err)
        higher_gluon_err.Add(higher_gluon2_err)
        lower_quark_err.Add(lower_quark2_err)
        lower_gluon_err.Add(lower_gluon2_err)

        #uncertainty propagation
        higher_quark_unc = unc_array_err(higher_quark,higher_quark_err)
        higher_gluon_unc = unc_array_err(higher_gluon,higher_gluon_err)
        lower_quark_unc = unc_array_err(lower_quark,lower_quark_err)
        lower_gluon_unc = unc_array_err(lower_gluon,lower_gluon_err)
        
        higher_data_unc = unc_array(higher_data)
        lower_data_unc = unc_array(lower_data)
        
        ToT_Fq2_unc = higher_quark_unc.sum()
        ToT_Fg2_unc = higher_gluon_unc.sum()

        ToT_Cq2_unc = lower_quark_unc.sum()
        ToT_Cg2_unc = lower_gluon_unc.sum()

        # calculate the fraction of forward(higher) / central(lower) quark or gluon jet
        fg_unc=ToT_Fg2_unc/(ToT_Fg2_unc+ToT_Fq2_unc)
        cg_unc=ToT_Cg2_unc/(ToT_Cq2_unc+ToT_Cg2_unc)
        fq_unc=1.-fg_unc
        cq_unc=1.-cg_unc

        factor_quark_unc = lower_quark_unc
        factor_gluon_unc = lower_gluon_unc

        
        #First normalize it 
        higher_quark_unc = higher_quark_unc/higher_quark_unc.sum()
        higher_gluon_unc = higher_gluon_unc/higher_gluon_unc.sum()
        lower_quark_unc = lower_quark_unc/lower_quark_unc.sum()
        lower_gluon_unc = lower_gluon_unc/lower_gluon_unc.sum()
        higher_data_unc = higher_data_unc/higher_data_unc.sum()
        lower_data_unc = lower_data_unc/lower_data_unc.sum()
 
        higher_quark_hist = higher_quark.Clone()
        higher_gluon_hist = higher_quark.Clone() 
        lower_quark_hist = higher_quark.Clone()
        lower_gluon_hist = higher_quark.Clone()
        higher_data_hist = higher_quark.Clone()
        lower_data_hist = higher_quark.Clone()   
        

        if (doreweight=="Quark"):
                for i in range(1,higher_quark.GetNbinsX()+1):
                        if (lower_quark.GetBinContent(i) > 0 and lower_gluon.GetBinContent(i) > 0):
                                factor_gluon_unc[i-1] = higher_gluon_unc[i-1]/lower_gluon_unc[i-1]
                                factor_quark_unc[i-1] = higher_quark_unc[i-1]/lower_quark_unc[i-1]
                        else:
                                factor_gluon_unc[i-1] = unc.ufloat(1, 0)
                                factor_quark_unc[i-1] = unc.ufloat(1, 0)


                lower_data_unc=lower_data_unc*factor_quark_unc

        if (doreweight=="Gluon"):
                for i in range(1,higher_quark.GetNbinsX()+1):
                        if (lower_quark.GetBinContent(i) > 0 and lower_gluon.GetBinContent(i) > 0):
                                #print i,higher_quark.GetBinContent(i)/lower_quark.GetBinContent(i),higher_gluon.GetBinContent(i)/lower_gluon.GetBinContent(i)
                                factor_gluon_unc[i-1] = higher_gluon_unc[i-1]/lower_gluon_unc[i-1]
                                factor_quark_unc[i-1] = higher_quark_unc[i-1]/lower_quark_unc[i-1]
                        else:
                                factor_gluon_unc[i-1] = unc.ufloat(1, 0)
                                factor_quark_unc[i-1] = unc.ufloat(1, 0)
  

                lower_data_unc=lower_data_unc*factor_gluon_unc
                
        for i in range(1,higher_quark.GetNbinsX()+1):
            higher_quark_hist.SetBinContent(i,higher_quark_unc[i-1].nominal_value)
            higher_quark_hist.SetBinError(i,higher_quark_unc[i-1].std_dev)
            lower_quark_hist.SetBinContent(i,lower_quark_unc[i-1].nominal_value)
            lower_quark_hist.SetBinError(i,lower_quark_unc[i-1].std_dev)
            higher_gluon_hist.SetBinContent(i,higher_gluon_unc[i-1].nominal_value)
            higher_gluon_hist.SetBinError(i,higher_gluon_unc[i-1].std_dev)
            lower_gluon_hist.SetBinContent(i,lower_gluon_unc[i-1].nominal_value)
            lower_gluon_hist.SetBinError(i,lower_gluon_unc[i-1].std_dev)
                    

        factor_quark_hist = higher_quark.Clone()
        factor_gluon_hist = higher_gluon.Clone()
        for i in range(1,higher_quark.GetNbinsX()+1):
            
            factor_quark_hist.SetBinContent(i,factor_quark_unc[i-1].nominal_value)
            factor_quark_hist.SetBinError(i,factor_quark_unc[i-1].std_dev)
            factor_gluon_hist.SetBinContent(i,factor_gluon_unc[i-1].nominal_value)
            factor_gluon_hist.SetBinError(i,factor_gluon_unc[i-1].std_dev)
            
        gStyle.SetOptStat(0)
        
        c.Divide(2,1)

        top = c.cd(1)
        top.SetPad(0.0,0.0,1.0,1.0)
        top.SetFillColor(0)
        top.SetBorderMode(0)
        top.SetBorderSize(2)
        top.SetTickx(1)
        top.SetTicky(1)
        top.SetLeftMargin(0.14)
        top.SetRightMargin(0.055)
        top.SetBottomMargin(0.3)#0.25
        top.SetFrameBorderMode(0)
        #top.SetLogy(1)
        top.cd()
        
        higher_quark_hist.SetMarkerColor(4)
        higher_quark_hist.SetLineColor(4)
        higher_quark_hist.SetMarkerSize(0.5)
        higher_quark_hist.SetLineStyle(1)
            
        higher_gluon_hist.SetMarkerColor(2)
        higher_gluon_hist.SetLineColor(2)
        higher_gluon_hist.SetMarkerSize(0.5)
        higher_gluon_hist.SetLineStyle(1)         
        
        lower_quark_hist.SetMarkerColor(4)
        lower_quark_hist.SetLineColor(4)
        lower_quark_hist.SetMarkerSize(0.5)
        lower_quark_hist.SetLineStyle(2)
            
        lower_gluon_hist.SetMarkerColor(2)
        lower_gluon_hist.SetLineColor(2)
        lower_gluon_hist.SetMarkerSize(0.5)
        lower_gluon_hist.SetLineStyle(2)           

        
        bot = c.cd(2)
        bot.SetPad(0.0,0.0,1.0,0.3)
        bot.SetFillColor(0)
        bot.SetBorderMode(0)
        bot.SetBorderSize(2)
        bot.SetTickx(1)
        bot.SetTicky(1)
        bot.SetLeftMargin(0.14)
        bot.SetRightMargin(0.055)
        bot.SetTopMargin(0.045)
        bot.SetBottomMargin(0.4)
        bot.SetFrameBorderMode(0)        
        
        factor_quark_hist.SetMarkerColor(4)
        factor_quark_hist.SetLineColor(4)
        factor_quark_hist.SetMarkerSize(0.5)
        factor_quark_hist.SetLineStyle(1)
            
        factor_gluon_hist.SetMarkerColor(2)
        factor_gluon_hist.SetLineColor(2)
        factor_gluon_hist.SetMarkerSize(0.5)
        factor_gluon_hist.SetLineStyle(1)   
        #leg.AddEntry(gluon_data,"Extracted gluon (data)","p")

        factor_quark_hist.SetMinimum(0)
        factor_quark_hist.SetMaximum(2)
        
        factor_quark_hist.GetYaxis().SetTitle("Forward/Central")        
        factor_quark_hist.GetYaxis().SetRangeUser(0.7,1.3)
        factor_quark_hist.GetXaxis().SetTitleOffset(1)
        factor_quark_hist.GetXaxis().SetTitleSize(0.11)
        factor_quark_hist.GetXaxis().SetLabelSize(0.1)
        factor_quark_hist.GetXaxis().SetLabelOffset(0.03)
        factor_quark_hist.GetYaxis().SetTitleSize(0.1)
        factor_quark_hist.GetYaxis().SetTitleOffset(0.5)
        factor_quark_hist.GetYaxis().SetLabelOffset(0.01)
        top.cd()
        rmax = higher_gluon_hist.GetMaximum()*1.5
        higher_quark_hist.GetYaxis().SetTitle("Normalized to unity")        
        higher_quark_hist.SetMaximum(rmax)
        higher_quark_hist.Draw("HIST")
        higher_gluon_hist.Draw("HIST same")
        lower_quark_hist.Draw("HIST same")
        lower_gluon_hist.Draw("HIST same")
        
        if(inputvar == "ntrk"):
            factor_quark_hist.GetXaxis().SetTitle("N_{track}")
            leg = TLegend(0.6,0.7,0.9,0.9) ##0.6,0.5,0.9,0.7  
            
        if(inputvar == "bdt"):
            factor_quark_hist.GetXaxis().SetTitle("BDT")
            leg = TLegend(0.6,0.7,0.9,0.9) ##0.6,0.5,0.9,0.7  
        leg.AddEntry(higher_quark_hist,"forward quark","l")
        leg.AddEntry(lower_quark_hist,"central quark","l")     
        leg.AddEntry(higher_gluon_hist,"forward gluon","l")
        leg.AddEntry(lower_gluon_hist,"central gluon","l")  
        leg.SetTextFont(42)
        leg.SetFillColor(0)
        leg.SetBorderSize(0)
        leg.SetFillStyle(0)
        leg.SetNColumns(1)
        leg.Draw()        
        myText(0.18,0.75,"#bf{#scale[1.5]{pT range: "+str(min)+" - "+str(max)+" GeV}}")
        
        bot.cd()
        factor_quark_hist.Draw("HIST e")
        factor_gluon_hist.Draw("HIST e same")
        c.Print("./plots_"+var+"/"+str(min)+"_"+mc+"_"+var+"_distribution.png")
