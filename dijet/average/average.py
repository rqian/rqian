from ROOT import *
import numpy as np

doreweight = 0   #decide if we want to do the reweighting process

var = "c1"  #change the var name according to the inputvar you want to read
mc = "pythia_MC"   #by setting it as "SF" or "MC", it will automatically making scale factor plots or MC closure plots
inputvar = var  #by setting it as bdt (or ntrk,width,c1..), it will read the corresponding histogram, but remember to change the TLine range according to X-axis of different variable, one can check it by browsing the histograms in root file.

def myText(x,y,text, color = 1):
	l = TLatex()
	l.SetTextSize(0.025)
	l.SetNDC()
	l.SetTextColor(color)
	l.DrawLatex(x,y,text)
	pass

bin = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]
bins = np.array([500.,600.,800.,1000.,1200.,1500.,2000.])

qmcerror = []
gmcerror = []
nstraps = 10

hq = TH1F("hq","",6,bins)
hg = TH1F("hg","",6,bins)
q = TH1F("q","",6,bins)
g = TH1F("g","",6,bins)

if "pythia" in mc:
	ntrackall = TFile("../root-files/dijet-pythia-py.root")
if "sherpa" in mc:
	ntrackall = TFile("../root-files/dijet-sherpa-py-nan.root")
ntrackall3 = TFile("../root-files/dijet_data_py.root")

for i in range(7,13):   #for only dijet event, start from jet pT>500 GeV
#for i in range(13):	#for gamma+jet combined with dijet event, start from jet pT>0 GeV
        min = bin[i]
        max = bin[i+1]
        qvals = []
        gvals = []
        qexvals = []
        gexvals = []
        k=i

        if(min != 800):
                higher_quark2 = ntrackall.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar)
                higher_gluon2 = ntrackall.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar)
                higher_data2 = ntrackall3.Get(str(min)+"_LeadingJet_Forward_Data_"+inputvar)
                lower_quark2 = ntrackall.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar)
                lower_gluon2 = ntrackall.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar)
                lower_data2 = ntrackall3.Get(str(min)+"_LeadingJet_Central_Data_"+inputvar)

        higher_quark = ntrackall.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar)
        higher_gluon = ntrackall.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar)

        lower_quark = ntrackall.Get(str(min)+"_SubJet_Central_Quark_"+inputvar)
        lower_gluon = ntrackall.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar)

        higher_data = ntrackall3.Get(str(min)+"_SubJet_Forward_Data_"+inputvar)
        lower_data = ntrackall3.Get(str(min)+"_SubJet_Central_Data_"+inputvar)

        #add leading and subleading jet from only dijet event together,
        #note that for gammajet+dijet event, we need to add leading jet from gammajet and leading jet from dijet sample together
        higher_data.Add(higher_data2)
        lower_data.Add(lower_data2)
        higher_quark.Add(higher_quark2)
        higher_gluon.Add(higher_gluon2)
        lower_quark.Add(lower_quark2)
        lower_gluon.Add(lower_gluon2)

        #Clone histograms for bootstrapping to preserve from destructive edits
        higherq = higher_quark.Clone("")
        print(higher_quark.GetBinContent(20),higherq.GetBinContent(20))
        higherg = higher_gluon.Clone()
        lowerq = lower_quark.Clone()
        lowerg = lower_gluon.Clone()

        ToT_Fq2 = 0.
        ToT_Fg2 = 0.

        ToT_Cq2 =0.
        ToT_Cg2 = 0.

        for j in range(1,lower_quark.GetNbinsX()+1):
                ToT_Fq2+=higher_quark.GetBinContent(j)
                ToT_Cq2+=lower_quark.GetBinContent(j)
                ToT_Fg2+=higher_gluon.GetBinContent(j)
                ToT_Cg2+=lower_gluon.GetBinContent(j)

        # calculate the fraction of forward(higher) / central(lower) quark or gluon jet
        fg=ToT_Fg2/(ToT_Fg2+ToT_Fq2)
        cg=ToT_Cg2/(ToT_Cq2+ToT_Cg2)
        fq=1.-fg
        cq=1.-cg

        if (doreweight):
                for i in range(1,higher_quark.GetNbinsX()+1):
                        if (lower_quark.GetBinContent(i) > 0 and lower_gluon.GetBinContent(i) > 0):
                                #print i,higher_quark.GetBinContent(i)/lower_quark.GetBinContent(i),higher_gluon.GetBinContent(i)/lower_gluon.GetBinContent(i)
                                factor_gluon = higher_gluon.GetBinContent(i)/lower_gluon.GetBinContent(i)
                                factor_quark = higher_quark.GetBinContent(i)/lower_quark.GetBinContent(i)
                                lower_quark.SetBinContent(i,lower_quark.GetBinContent(i)*factor_quark)
                                lower_gluon.SetBinContent(i,lower_gluon.GetBinContent(i)*factor_quark)
                                lower_data.SetBinContent(i,lower_data.GetBinContent(i)*factor_quark)
                                pass
                        pass
                pass

        if (lower_quark.Integral() != 0):
                lower_quark.Scale(1./lower_quark.Integral())
        if(lower_gluon.Integral() != 0):
                lower_gluon.Scale(1./lower_gluon.Integral())
        if(higher_quark.Integral() != 0):
                higher_quark.Scale(1./higher_quark.Integral())
        if(higher_gluon.Integral() != 0):
                higher_gluon.Scale(1./higher_gluon.Integral())
        if(lower_data.Integral() != 0):
                lower_data.Scale(1./lower_data.Integral())
        if(higher_data.Integral() != 0):
                higher_data.Scale(1./higher_data.Integral())

        higher = higher_quark.Clone("")
        lower = higher_quark.Clone("")

        for i in range(1,higher.GetNbinsX()+1):
                higher.SetBinContent(i,fg*higher_gluon.GetBinContent(i)+fq*higher_quark.GetBinContent(i))
                lower.SetBinContent(i,cg*lower_gluon.GetBinContent(i)+cq*lower_quark.GetBinContent(i))
                pass

        #Now, let's solve.

        quark = higher_quark.Clone("")
        gluon = higher_quark.Clone("")
        quark_data = higher_data.Clone("")
        gluon_data = higher_data.Clone("")

        #Matrix method here
        for i in range(1,higher.GetNbinsX()+1):
                F = higher.GetBinContent(i)
                C = lower.GetBinContent(i)
                if((cg*fq-fg*cq) != 0 ):
                        Q = -(C*fg-F*cg)/(cg*fq-fg*cq)
                        G = (C*fq-F*cq)/(cg*fq-fg*cq)
                        quark.SetBinContent(i,Q)
                        gluon.SetBinContent(i,G)
                        #print "   ",i,G,higher_gluon.GetBinContent(i),lower_gluon.GetBinContent(i)
                pass

        #Bootstrapped matrix method
        for j in range(0,nstraps):
                qtemp = []
                gtemp = []
                qextemp = []
                gextemp = []
                for i in range(1,higher.GetNbinsX()+1):
                        h_q = np.random.poisson(higherq.GetBinContent(i))
                        h_g = np.random.poisson(higherg.GetBinContent(i))
                        l_q = np.random.poisson(lowerq.GetBinContent(i))
                        l_g = np.random.poisson(lowerg.GetBinContent(i))

                        qtemp.append(h_q+l_q)
                        gtemp.append(h_g+l_g)
                        if((cg*fq-fg*cq) != 0 ):
                                qextemp.append(-(C*fg-F*cg)/(cg*fq-fg*cq))
                                gextemp.append((C*fq-F*cq)/(cg*fq-fg*cq))
                # mean of extract qg value
                a = np.mean(qtemp)
                b = np.mean(gtemp)
                c = np.mean(qextemp)
                d = np.mean(gextemp)

                print(j,a,b,c,d)
        # a,b is the mean of mc , c,d is the mean of mc extracted q/g
        a = higher_quark.GetMean()
        b = higher_gluon.GetMean()
        c = quark.GetMean()
        d = gluon.GetMean()
        
        print(k-6,a,b,c,d)

        hq.SetBinContent(k-6,a)
        hg.SetBinContent(k-6,b)
        q.SetBinContent(k-6,c)
        g.SetBinContent(k-6,d)

quark_ratio = q.Clone()
gluon_ratio = g.Clone()
quark_ratio.Divide(hq)
gluon_ratio.Divide(hg)
for i in range(6):
    print(quark_ratio.GetBinContent(i))

c1 = TCanvas("c1","c1",500,500)

gStyle.SetOptStat(0)
gPad.SetLogx(1)
######################## for ratio plot
c1.Divide(2,1)

top = c1.cd(1)
top.SetPad(0.0,0.0,1.0,1.0)
top.SetFillColor(0)
top.SetBorderMode(0)
top.SetBorderSize(2)
top.SetTickx(1)
top.SetTicky(1)
top.SetLeftMargin(0.14)
top.SetRightMargin(0.055)
top.SetBottomMargin(0.3)#0.25
top.SetFrameBorderMode(0)
#top.SetLogy(1)

bot = c1.cd(2)
bot.SetPad(0.0,0.0,1.0,0.3)
bot.SetFillColor(0)
bot.SetBorderMode(0)
bot.SetBorderSize(2)
bot.SetTickx(1)
bot.SetTicky(1)
bot.SetLeftMargin(0.14)
bot.SetRightMargin(0.055)
bot.SetTopMargin(0.045)
bot.SetBottomMargin(0.4)
bot.SetFrameBorderMode(0)

hq.SetTitle("")
hq.GetXaxis().SetTitle(var)
hq.GetYaxis().SetTitle("Normalized to unity")
hq.GetYaxis().SetNdivisions(505)
hq.GetYaxis().SetRangeUser(-0.01,hg.GetMaximum()*1.5)
hq.SetMarkerColor(4)
hq.SetLineColor(4)

q.SetMarkerColor(4)
q.SetLineColor(4)
q.SetMarkerSize(0.8)
q.SetMarkerStyle(20)

hg.SetMarkerColor(2)
hg.SetLineColor(2)

g.SetMarkerColor(2)
g.SetLineColor(2)
g.SetMarkerSize(0.8)
g.SetMarkerStyle(20)

quark_ratio.SetTitle("")
quark_ratio.GetYaxis().SetRangeUser(0.7,1.3)
quark_ratio.GetXaxis().SetTitle(var)
quark_ratio.GetXaxis().SetTitleOffset(1)
quark_ratio.GetXaxis().SetTitleSize(0.11)
quark_ratio.GetXaxis().SetLabelSize(0.1)
quark_ratio.GetXaxis().SetLabelOffset(0.03)
quark_ratio.GetYaxis().SetTitleSize(0.1)
quark_ratio.GetYaxis().SetTitleOffset(0.5)
quark_ratio.GetYaxis().SetLabelOffset(0.01)

quark_ratio.SetMarkerColor(4)
quark_ratio.SetMarkerSize(0.8)
quark_ratio.SetMarkerStyle(20)

gluon_ratio.SetMarkerColor(2)
gluon_ratio.SetMarkerSize(0.8)
gluon_ratio.SetMarkerStyle(20)

top.cd()
hq.Draw("hist")

leg = TLegend(0.6,0.7,0.9,0.9) ##0.6,0.5,0.9,0.7
leg.SetTextFont(42)
leg.SetFillColor(0)
leg.SetBorderSize(0)
leg.SetFillStyle(0)
leg.SetNColumns(1)
leg.AddEntry(q,"Quark (Extracted)","p")
leg.AddEntry(g,"Gluon (Extracted)","p")
leg.AddEntry(hq,"Quark (Higher |\eta|)","l")
leg.AddEntry(hg,"Gluon (Higher |\eta|)","l")
leg.Draw()

myText(0.18,0.84,"#it{#bf{#scale[1.8]{#bf{ATLAS} Simulation Internal}}}")
myText(0.18,0.80,"#bf{#scale[1.5]{#sqrt{s} = 13 TeV}}")


line = TLine(500.,1,2000.,1)

hg.Draw("hist same")
q.Draw("p same")
g.Draw("p same")

bot.cd()
quark_ratio.Draw("p")
gluon_ratio.Draw("p same")
line.Draw("p same")

c1.Print(str(var)+"_"+mc+".pdf")

