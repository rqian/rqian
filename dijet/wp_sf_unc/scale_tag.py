###rescale gammajet mc and data
from ROOT import *
import numpy as np

# When quark efficiency is 0.6 ,using sherpa dijet sample as normalization ,calculating higer mc/higher data as scale factor, plotting scale factor , gluon rejection, and uncertainty. 

doreweight = 0   #decide if we want to do the reweighting process

var = "pt"  #change the var name according to the inputvar you want to read
mc = "sherpa_SF"   #by setting it as "SF" or "MC", it will automatically making scale factor plots or MC closure plots
inputvar = var  #by setting it as bdt (or ntrk,width,c1..), it will read the corresponding histogram, but remember to change the TLine range according to X-axis of different variable, one can check it by browsing the histograms in root file.

bin = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]

sherpa = TFile('dijet_sherpa_py_tag_0.7.root')
sherpa_lund = TFile('dijet_sherpa_lund_py_tag_0.8.root')
data = TFile('dijet_data_0.6_uproot_wp.root')
herang = TFile('dijet_herang_py_tag_0.7mc.root')
herdipo = TFile('dijet_herdipo_py_tag_0.7.root')

mc_sherpa = 0
mc_sherpa_lund = 0
mc_herang = 0
mc_herdipo = 0
data_sum = 0

def printhis(his):
    for i in range(1,61):
        print(i,his.GetBinContent(i))

def compare_his(his1,his2):
    for i in range(1,61):
        print(i,his1.GetBinContent(i),his2.GetBinContent(i))

for i in range(7,13):   #for only dijet event, start from jet pT>500 GeV
#for i in range(13):	#for gamma+jet combined with dijet event, start from jet pT>0 GeV
    min = bin[i]
    max = bin[i+1]
    print(i)
    #sherpa
    sherpa_quark = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    sherpa_quark2 = sherpa.Get(str(bin[i])+'_LeadingJet_Central_Quark_bdt')    
    sherpa_gluon = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    sherpa_gluon2 = sherpa.Get(str(bin[i])+'_LeadingJet_Central_Gluon_bdt')
    sherpa_lund_quark = sherpa_lund.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    sherpa_lund_quark2 = sherpa_lund.Get(str(bin[i])+'_LeadingJet_Central_Quark_bdt')    
    sherpa_lund_gluon = sherpa_lund.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    sherpa_lund_gluon2 = sherpa_lund.Get(str(bin[i])+'_LeadingJet_Central_Gluon_bdt')
    herang_quark = herang.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    herang_quark2 = herang.Get(str(bin[i])+'_LeadingJet_Central_Quark_bdt')    
    herang_gluon = herang.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    herdipo_gluon2 = herang.Get(str(bin[i])+'_LeadingJet_Central_Gluon_bdt')
    herdipo_quark = herdipo.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    herdipo_quark2 = herdipo.Get(str(bin[i])+'_LeadingJet_Central_Quark_bdt')    
    herdipo_gluon = herdipo.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    herdipo_gluon2 = herdipo.Get(str(bin[i])+'_LeadingJet_Central_Gluon_bdt')
    data1 = data.Get(str(bin[i])+'_LeadingJet_Forward_Quark_Data_bdt')
    data2 = data.Get(str(bin[i])+'_LeadingJet_Central_Quark_Data_bdt')
    data3 = data.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_Data_bdt')
    data4 = data.Get(str(bin[i])+'_LeadingJet_Central_Gluon_Data_bdt')
    #reassigning to variables
    sherpa_quark.Add(sherpa_quark2)
    sherpa_gluon.Add(sherpa_gluon)
    sherpa_quark.Add(sherpa_gluon)

    sherpa_lund_quark.Add(sherpa_lund_quark2)
    sherpa_lund_gluon.Add(sherpa_lund_gluon)
    sherpa_lund_quark.Add(sherpa_lund_gluon)

    herang_quark.Add(herang_quark2)
    herang_gluon.Add(herang_gluon)
    herang_quark.Add(herang_gluon)        

    herdipo_quark.Add(herdipo_quark2)
    herdipo_gluon.Add(herdipo_gluon)
    herdipo_quark.Add(herdipo_gluon)

    data1.Add(data2)
    data3.Add(data4)
    data1.Add(data3)

    mc_sherpa += sherpa_quark.Integral(1,61)
    mc_sherpa_lund += sherpa_lund_quark.Integral(1,61)
    mc_herang += herang_quark.Integral(1,61)
    mc_herdipo += herdipo_quark.Integral(1,61)
    data_sum += data1.Integral(1,61)
   
     

print("mc_sherpa = " + str(mc_sherpa))
print("data = " + str(data_sum))
print("mc_sherpa_lund = " + str(mc_sherpa_lund))
print("mc_herang = " + str(mc_herang))
print("mc_herdipo = " + str(mc_herdipo))

##mc_sherpa = 60867212.25321388
##data = 26836731.0
##mc_sherpa_lund = 59324752.03712411
##mc_herang = 61520326.37292723
##mc_herdipo = 31143574.65059941


