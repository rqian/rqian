###rescale gammajet mc and data
from ROOT import *
import numpy as np

# When quark efficiency is 0.6 ,using sherpa dijet sample as normalization ,calculating higer mc/higher data as scale factor, plotting scale factor , gluon rejection, and uncertainty. 

doreweight = 0   #decide if we want to do the reweighting process

var = "pt"  #change the var name according to the inputvar you want to read
mc = "sherpa_SF"   #by setting it as "SF" or "MC", it will automatically making scale factor plots or MC closure plots
inputvar = var  #by setting it as bdt (or ntrk,width,c1..), it will read the corresponding histogram, but remember to change the TLine range according to X-axis of different variable, one can check it by browsing the histograms in root file.

bin = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]

sherpa = TFile('./dijet_sherpa_py_tag.root')

mc_sherpa=0
data_sum = 0
def printhis(his):
    for i in range(1,61):
        print(i,his.GetBinContent(i))

for i in range(7,13):   #for only dijet event, start from jet pT>500 GeV
#for i in range(13):	#for gamma+jet combined with dijet event, start from jet pT>0 GeV
    min = bin[i]
    max = bin[i+1]
    print(i)
    #sherpa
    sherpa_quark = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    sherpa_quark2 = sherpa.Get(str(bin[i])+'_LeadingJet_Central_Quark_bdt')    
    sherpa_gluon = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    sherpa_gluon2 = sherpa.Get(str(bin[i])+'_LeadingJet_Central_Gluon_bdt')



    #reassigning to variables
    sherpa_quark.Add(sherpa_quark2)
    sherpa_quark.Add(sherpa_gluon)
    sherpa_quark.Add(sherpa_gluon2)
    higher_quark = sherpa_quark.Clone("")

    mc_sherpa += higher_quark.Integral(1,61)



print("mc_sherpa = " + str(mc_sherpa))


##mc_sherpa = 443.3584116777705
##data = 46769486.0

