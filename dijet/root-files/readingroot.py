from ROOT import *
import numpy as np


def myText(x,y,text, color = 1):
	l = TLatex()
	l.SetTextSize(0.025)
	l.SetNDC()
	l.SetTextColor(color)
	l.DrawLatex(x,y,text)
	pass



bins = np.array([500.,600.,800.,1000.,1200.,1500.,2000.])
bin = np.array([500,600,800,1000,1200,1500,2000])
qeff = [0.60000,0.59999,0.60000,0.59999,0.60000,0.60000]
grej = [0.16977,0.16416,0.15935,0.15481,0.14677,0.14660]
#qsf = [1.076,1.076,1.067,0.986,1.007,0.837] #Left
#gsf = [0.926,0.918,0.933,1.010,1.083,1.998] #Left
#qsf = [-39.,3.54,3.16,0.92,0.85,0.47] #Right
#gsf = [1.34,1.22,1.20,0.93,0.90,0.75] #Right
#qsf = [0.801,0.821,0.804,0.932,0.921,0.969] #Right, no wp cut
#gsf = [1.187,1.116,1.091,0.918,0.885,0.783] #Right, no wp cut

sherpa = TFile('../root-files/dijet_sherpa_py_tag.root')
data = TFile('../root-files/dijet_data_py_wp.root')
sherpa_lund = TFile('../root-files/dijet_sherpa_lund_py_tag.root')
powpyt = TFile('../root-files/dijet_powpyt_py_tag.root')
pythia = TFile('../root-files/dijet_pythia_a_py_tag.root')
herdipo = TFile('../root-files/dijet_herdipo_py_tag.root')
herang = TFile('../root-files/dijet_herang_a_py_tag.root')

def Read(his1,his2):
    for i in range(1,61):
        print(i,his1.GetBinContent(i),his2.GetBinContent(i))

for i in range(4,5):
    his1 =     sherpa_gluon = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    his2 =         sherpa_quark = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    Read(his1,his2)

