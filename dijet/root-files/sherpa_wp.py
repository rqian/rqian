##!/eos/user/e/esaraiva/uproot/venv/bin/python
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import uproot3 as uproot
import numpy as np
import sys

working_point_eff = 0.6 #wokring point as a decimal
entrystep = int(1)

bins = [300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]  #this bin range is for only dijet event
#bins = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]  #this bin range is for gammajet+dijet event
arraymap = {
    '300': [],
    '400': [],
    '500': [],
    '600': [],
    '800': [],
    '1000': [],
    '1200': [],
    '1500': [],
    }
    
gluon_arraymap = {
    '300': [],
    '400': [],
    '500': [],
    '600': [],
    '800': [],
    '1000': [],
    '1200': [],
    '1500': [],
    }

xsec = [0.0,21400000000.0,89314000.0,9275100.0,55101.0,1631.4,128.4,27.212,0.206,0.036,0.0,0.0,0.0]
eff = [0.0,0.0014414,0.0051224,0.00056516,0.0014972,0.024206,0.01082,0.003618,0.015966,0.0033033,0.0,0.0,0.0]

print("sherpa \n")
	   
finput = open("/eos/user/e/esaraiva/dijet-systematics/sherpa_list.txt")
inputs = finput.read().splitlines()

nentries = np.zeros(13)
usefiles = []

for file in inputs:
    tr = uproot.open(file)["AntiKt4EMPFlow_dijet_insitu"]
    data = tr.array("mcChannelNumber")
    index = (data[0]-76) % 10
    #print(file,index,"\n")
    if(index >=1 and index <=4):
        entries = tr.numentries
        nentries[index] += entries
        #print("using entries \n")
    if(index > 4):
        mc_mod = data[0] % 10
        entries = uproot.open(file)["AntiKt4EMPFlow_J"+str(mc_mod)+"_sumOfWeights"].values[0]
        nentries[index] += entries
        #print("using weighted entries \n")
    usefiles.append(file)
        
#print(nentries,"\n")

def GetJetType(label):
	if label == -99:
		return "Data"
	elif label == 21:
		return "Gluon"
	elif label > 0 and label < 5:
		return "Quark"
	else:
		return "Other"
		
def JetpTBin(pt):
    for i in range(len(bins)):
        if(pt >= bins[i] and pt<bins[i+1]):
            return str(bins[i])
        if (pt<bins[0]):
            print("jet pT below minimum bin")
            return -1

#Unfourtunately I can't fully utilize the use of arrays because each jet must be matched with the corresponding histogram.
#for i in range():
def ReadTree(df,f):
    #global arraymap
    
    tr = uproot.open(f)["AntiKt4EMPFlow_dijet_insitu"]
    data = tr.array("mcChannelNumber")
    index = (data[0]-76) % 10
    
    for i in range(0,len(df["pass_HLT_j420"])):
        if(df["pass_HLT_j420"][i] == 1 and df["j1_pT"][i] > 500 and df["j1_pT"][i] < 2000 and abs(df["j1_eta"][i]) < 2.1 and abs(df["j2_eta"][i]) < 2.1 and df["j1_pT"][i]/df["j2_pT"][i] < 1.5):
            
            bin1 = JetpTBin(df["j1_pT"][i])
            bin2 = JetpTBin(df["j2_pT"][i])
            #print(df["j1_pT"][i],df["j2_pT"][i])
            #print(bin1,bin2)
            
            label1 = GetJetType(df["j1_partonLabel"][i])
            label2 = GetJetType(df["j2_partonLabel"][i])
            	
            total_weight = df["weight"][i]*xsec[index]*eff[index]/nentries[index]
            
            if(label1 == "Quark"):
                arraymap[str(bin1)].append([df["j1_bdt_resp"][i],total_weight])
            if(label2 == "Quark"):
                arraymap[str(bin2)].append([df["j2_bdt_resp"][i],total_weight])
            
            if(label1 == "Gluon"):
                gluon_arraymap[str(bin1)].append([df["j1_bdt_resp"][i],total_weight])
            if(label2 == "Gluon"):
                gluon_arraymap[str(bin2)].append([df["j2_bdt_resp"][i],total_weight])

######## read and excute TTree from root file 
#finput = TFile.Open("/eos/user/e/esaraiva/AQT_dijet_sherpa_bdt/dijet_sherpa_bdt_d.root")

#print(usefiles)
for file in usefiles:
    #print(file)
    tr = uproot.open(file)["AntiKt4EMPFlow_dijet_insitu"]
    data = tr.arrays(["pass_HLT_j420","j[12]_pT","j[12]_eta","j[12]_bdt_resp","weight","j[12]_partonLabel"])#this converts the tree to a     python dictionary with keys equal to the branch names and values equal to the branch data. This can be left blank to iport all of the data from all branches.
    ReadTree(data,file)


for key in arraymap.keys():
    #print(arraymap[key])
    lzip = np.array(arraymap[key])
    glzip = np.array(gluon_arraymap[key])
    if(len(arraymap[key])==0):
        print("Array length was 0")
        continue
    #print(lzip)
    #print("")
    w_list = np.zeros(len(lzip))
    bdt_list = np.zeros(len(lzip))
    gluon_w_list = np.zeros(len(glzip))
    gluon_bdt_list = np.zeros(len(glzip))
    
    ind = np.argsort(lzip[:,0])
    g_ind = np.argsort(glzip[:,0])
    #print(ind)
    #w_list[w_list[:,0].argsort()] 
    for i in range(len(w_list)):
        w_list[i] = lzip[ind[i]][1]
        bdt_list[i] = lzip[ind[i]][0]
    
    print(len(gluon_w_list),len(gluon_bdt_list),len(g_ind))
    
    for i in range(len(gluon_w_list)):
        gluon_w_list[i] = glzip[g_ind[i]][1]
        gluon_bdt_list[i] = glzip[g_ind[i]][0]
    
    w_list = np.cumsum(w_list)
    gluon_w_list = np.cumsum(gluon_w_list)
    
    idx = (np.abs(w_list - working_point_eff*w_list[-1])).argmin()
    
    print("index = "+str(idx))
    print("quark length = "+str(len(bdt_list))+",  gluon length = "+str(len(gluon_bdt_list)))
    print("quark eff = "+str(w_list[idx]/w_list[-1]))
        
    wp = bdt_list[idx]
    gidx = (np.abs(gluon_bdt_list - wp)).argmin()
    gr = gluon_w_list[gidx]
    print("gluon rej = "+str(gr/gluon_w_list[-1]))
    
    print(str(key)+","+str(wp)+","+str(w_list[idx]/w_list[-1])+","+str(gr/gluon_w_list[-1]))
